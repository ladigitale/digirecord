<?php

session_start();

require 'headers.php';

if (!empty($_POST['id']) && !empty($_POST['question']) && !empty($_POST['reponse']) && !empty($_POST['nouvellequestion']) && !empty($_POST['nouvellereponse'])) {
	require 'db.php';
	$id = $_POST['id'];
	$question = $_POST['question'];
	$reponse = strtolower($_POST['reponse']);
	$stmt = $db->prepare('SELECT question, reponse FROM digirecord_enregistrements WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else {
			$questionSecrete = $resultat[0]['question'];
			$reponseSecrete = $resultat[0]['reponse'];
			if ($question === $questionSecrete && password_verify($reponse, $reponseSecrete)) {
				$nouvellequestion = $_POST['nouvellequestion'];
				$nouvellereponse = password_hash(strtolower($_POST['nouvellereponse']), PASSWORD_DEFAULT);
				$stmt = $db->prepare('UPDATE digirecord_enregistrements SET question = :nouvellequestion, reponse = :nouvellereponse WHERE url = :url');
				if ($stmt->execute(array('nouvellequestion' => $nouvellequestion, 'nouvellereponse' => $nouvellereponse, 'url' => $id))) {
					$_SESSION['digirecord'][$id]['reponse'] = $nouvellereponse;
					echo 'acces_modifie';
				} else {
					echo 'erreur';
				}
			} else {
				echo 'non_autorise';
			}
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
