<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	$fichier = $_POST['fichier'];
	if (isset($_SESSION['digirecord'][$id]['reponse'])) {
		$reponse = $_SESSION['digirecord'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digirecord_enregistrements WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$stmt = $db->prepare('DELETE FROM digirecord_enregistrements WHERE url = :url');
			if ($stmt->execute(array('url' => $id))) {
				if (file_exists('../fichiers/' . $fichier)) {
					unlink('../fichiers/' . $fichier);
				}
				echo 'audio_supprime';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
