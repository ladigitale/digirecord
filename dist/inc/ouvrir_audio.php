<?php

session_start();

require 'headers.php';

if (!empty($_POST['audio']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$audio = $_POST['audio'];
	$question = $_POST['question'];
	$reponse = strtolower($_POST['reponse']);
	$stmt = $db->prepare('SELECT question, reponse, digidrive FROM digirecord_enregistrements WHERE url = :url');
	if ($stmt->execute(array('url' => $audio))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else {
			$questionSecrete = $resultat[0]['question'];
			$reponseSecrete = $resultat[0]['reponse'];
			if ($question === $questionSecrete && password_verify($reponse, $reponseSecrete)) {
				$_SESSION['digirecord'][$audio]['reponse'] = $reponseSecrete;
				$type = $_POST['type'];
				if ($type === 'api') {
					if (!isset($_SESSION['digirecord'][$audio]['digidrive'])) {
						$_SESSION['digirecord'][$audio]['digidrive'] = 1;
					}
					if ($resultat[0]['digidrive'] === 0) {
						$digidrive = 1;
						$stmt = $db->prepare('UPDATE digirecord_enregistrements SET digidrive = :digidrive WHERE url = :url');
						$stmt->execute(array('digidrive' => $digidrive, 'url' => $audio));
					}
				}
				echo 'audio_debloque';
			} else {
				echo 'non_autorise';
			}
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
