<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digirecord'][$id]['reponse'])) {
		$reponse = $_SESSION['digirecord'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digirecord_enregistrements WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($donnees = $stmt->fetchAll()) {
			$admin = false;
			if (count($donnees, COUNT_NORMAL) > 0 && $donnees[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$digidrive = 0;
			if (isset($_SESSION['digirecord'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digirecord'][$id]['digidrive'];
			} else if (intval($donnees[0]['digidrive']) === 1) {
				$digidrive = 1;
			}
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			if ($donnees[0]['vues'] !== '') {
				$vues = intval($donnees[0]['vues']);
			}
			if ($admin === false) {
				$vues = $vues + 1;
			}
			$stmt = $db->prepare('UPDATE digirecord_enregistrements SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
			if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
				echo json_encode(array('nom' => $donnees[0]['nom'], 'fichier' => $donnees[0]['fichier'], 'date' =>  $donnees[0]['date'], 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
			} else {
				echo 'erreur';
			}
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
