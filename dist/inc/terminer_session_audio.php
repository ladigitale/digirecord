<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	$id = $_POST['id'];
	unset($_SESSION['digirecord'][$id]['reponse']);
	echo 'session_terminee';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
