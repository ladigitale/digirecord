<?php

session_start();

require 'headers.php';

if (!empty($_FILES['blob']) && !empty($_POST['nom']) && !empty($_POST['question']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$id = uniqid('', false);
	$nom = $_POST['nom'];
	$question = $_POST['question'];
	$reponse = password_hash(strtolower($_POST['reponse']), PASSWORD_DEFAULT);
	$extension = pathinfo($_FILES['blob']['name'], PATHINFO_EXTENSION);
	$fichier = $id . '.' . $extension;
	$chemin = '../fichiers/' . $fichier;
	if (move_uploaded_file($_FILES['blob']['tmp_name'], $chemin)) {
		$date = date('Y-m-d H:i:s');
		$vues = 0;
		$digidrive = 0;
		$stmt = $db->prepare('INSERT INTO digirecord_enregistrements (url, nom, question, reponse, fichier, date, vues, derniere_visite, digidrive) VALUES (:url, :nom, :question, :reponse, :fichier, :date, :vues, :derniere_visite, :digidrive)');
		if ($stmt->execute(array('url' => $id, 'nom' => $nom, 'question' => $question, 'reponse' => $reponse, 'fichier' => $fichier, 'date' => $date, 'vues' => $vues, 'derniere_visite' => $date, 'digidrive' => $digidrive))) {
			$_SESSION['digirecord'][$id]['reponse'] = $reponse;
			echo $id;
		} else {
			echo 'erreur';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
