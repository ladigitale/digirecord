import { createRouter, createWebHashHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Lecteur from '../views/Lecteur.vue'

const routes = [
	{
		path: '/',
		name: 'Accueil',
		component: Accueil
	},
	{
		path: '/r/:id',
		name: 'Lecteur',
		component: Lecteur
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
